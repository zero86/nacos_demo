package com.demo.mybatis.service;

import com.demo.mybatis.entity.ThreeKingVo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 三国武将 服务类
 * </p>
 *
 * @author liumin
 * @since 2022-05-13
 */
public interface ThreeKingService extends IService<ThreeKingVo> {

}
