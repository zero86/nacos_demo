package com.demo.mybatis.service.impl;

import com.demo.mybatis.entity.ThreeKingVo;
import com.demo.mybatis.mapper.ThreeKingMapper;
import com.demo.mybatis.service.ThreeKingService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 三国武将 服务实现类
 * </p>
 *
 * @author liumin
 * @since 2022-05-13
 */
@Service
public class ThreeKingServiceImpl extends ServiceImpl<ThreeKingMapper, ThreeKingVo> implements ThreeKingService {

}
