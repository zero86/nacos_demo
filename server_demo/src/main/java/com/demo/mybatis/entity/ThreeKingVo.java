package com.demo.mybatis.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 三国武将
 * </p>
 *
 * @author liumin
 * @since 2022-05-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_three_king")
public class ThreeKingVo implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 姓名
     */
    private String name;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 身高
     */
    private Float height;

    /**
     * 体重
     */
    private Float weight;

    /**
     * 兵器评分
     */
    private Integer weapon;

    /**
     * 战马评分
     */
    private Integer horse;

    /**
     * 智
     */
    private Integer wisdom;

    /**
     * 信
     */
    @TableField("Integrity")
    private Integer Integrity;

    /**
     * 仁
     */
    private Integer humane;

    /**
     * 勇
     */
    private Integer brave;

    /**
     * 严
     */
    private Integer strict;

    private LocalDateTime updateTime;

    private LocalDateTime createTime;


}
