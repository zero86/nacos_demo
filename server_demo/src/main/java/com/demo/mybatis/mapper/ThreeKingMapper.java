package com.demo.mybatis.mapper;

import com.demo.mybatis.entity.ThreeKingVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 三国武将 Mapper 接口
 * </p>
 *
 * @author liumin
 * @since 2022-05-13
 */
public interface ThreeKingMapper extends BaseMapper<ThreeKingVo> {

}
