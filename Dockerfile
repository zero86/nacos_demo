FROM openjdk:8-jdk-oraclelinux8
#JAVA_HOME=/usr/local/openjdk-8

LABEL maintainer=mint

#同步本地时间&设置时区
RUN /bin/cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
RUN echo 'Asia/Shanghai' >/etc/timezone

#定义工作目录
WORKDIR /server

RUN mkdir -p /server

#复制代码文件内容到容器
COPY server_demo/target/*.jar /server/app.jar

#定义配置挂载点
#VOLUME ["/server/log"]

#暴露端口
EXPOSE 8081

#在配置里面的文件环境变量不生效
ENTRYPOINT ["/bin/bash", "-c", "java -Dspring.cloud.nacos.config.server-addr=$NACOS_META -jar /server/app.jar"]
#ENTRYPOINT ["/bin/bash", "-c", "java -jar /server/app.jar"]

#mvn clean package
#docker build -t nacos_demo:0.0.1 ./
#docker run --name nacos_demo -e NACOS_META=172.16.5.210:8849 -d nacos_demo:0.0.1


